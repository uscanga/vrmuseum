﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Laser : MonoBehaviour {

	//ray cast
	/*public*/ LineRenderer line;
	public RaycastHit laserHit;
	SteamVR_Controller.Device device;
	SteamVR_TrackedObject controller;
	bool startLaser;
	Vector2 touchpad;
	public Material green;
	public Material red;
	// Use this for initialization
	void Start () {
		line = gameObject.GetComponent<LineRenderer> ();
		//line.enabled = false;
		controller = gameObject.GetComponent<SteamVR_TrackedObject>();
		startLaser = false;
	}


	// Update is called once per frame
	void Update () {
		device = SteamVR_Controller.Input((int)controller.index);
		Vector3 lightCoor = Vector3.zero;
		//When the user presses the trigger the laser will appear. 
		if (device.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) {
			startLaser = !startLaser;
		}
		if (startLaser) {
			line.enabled = true;
			Ray laser;
			laser = new Ray (transform.position, transform.forward);
			line.SetPosition (0, laser.origin);
			line.material = red;
			
	

			if (Physics.Raycast (laser, out laserHit, 200)) {
				line.SetPosition (1, laserHit.point);

                //TODO: get button press in addition to collision to trigger actions
                if (laserHit.collider.tag == "painting") {
                    line.material = green;
                    if (device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad)) {
                        touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
                        laserHit.collider.transform.position += laserHit.collider.transform.up * Time.deltaTime * (touchpad.y * 5f);
                        Vector3 org = laserHit.collider.transform.position;
                        laserHit.collider.transform.position += laserHit.collider.transform.forward * Time.deltaTime * (touchpad.x * 5f);
                    }

                } else if (laserHit.collider.tag == "painting1") {
                    line.material = green;
                    if (device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad))
                    {
                        touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
                        laserHit.collider.transform.position += laserHit.collider.transform.forward * Time.deltaTime * (-touchpad.y * 5f);
                        Vector3 org = laserHit.collider.transform.position;
                        laserHit.collider.transform.position += laserHit.collider.transform.right * Time.deltaTime * (-touchpad.x * 5f);
                    }

                } else if (laserHit.collider.tag == "lightbulb") {
                    line.material = green;
                    if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip)) {
                        laserHit.collider.GetComponentInChildren<Light>().intensity += 1;
                        if (laserHit.collider.GetComponentInChildren<Light>().intensity == 8)
                            laserHit.collider.GetComponentInChildren<Light>().intensity = 0;
                    }
                    if (device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad)) {
                        touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
                        laserHit.collider.transform.position += GameObject.FindGameObjectWithTag("MainCamera").transform.forward * Time.deltaTime * (touchpad.y * 5f);
                        //Intensity for up and down
                        ///laserHit.collider.GetComponentInChildren<Light>().intensity += Time.deltaTime * (-touchpad.y * 5f);
                        laserHit.collider.transform.position += GameObject.FindGameObjectWithTag("MainCamera").transform.right * Time.deltaTime * (touchpad.x * 5f);
                        if (laserHit.collider.transform.position.y != 4.187f) {
                            Vector3 settingY = laserHit.collider.transform.position;
                            settingY.y = 4.187f;
                            laserHit.collider.transform.position = settingY;
                        }

                    }

                } else if (laserHit.collider.tag == "sculpture") {
                    line.material = green;
                    if (device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad)) {
                        touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

                        //I have to save the y value because the orientation affects the y value which should not happen
                        Vector3 originalY=laserHit.collider.transform.position;
                        
                        laserHit.collider.transform.position += GameObject.FindGameObjectWithTag("MainCamera").transform.forward * Time.deltaTime * (touchpad.y * 5f);
                        laserHit.collider.transform.position += GameObject.FindGameObjectWithTag("MainCamera").transform.right * Time.deltaTime * (touchpad.x * 5f);

                        /*if (laserHit.collider.transform.position.y != .79f) {
                            Vector3 settingY = laserHit.collider.transform.position;
                            settingY.y = .79f;
                            laserHit.collider.transform.position = settingY;
                        }*/
                        if (laserHit.collider.transform.position.y != originalY.y)
                        {
                            Vector3 settingY = laserHit.collider.transform.position;
                            settingY.y = originalY.y;
                            laserHit.collider.transform.position = settingY;
                        }
                    }
                }

					 
			} else {
				line.SetPosition (1, laser.GetPoint (200));

			}
		}
		else{
			line.enabled = false;
		}

	}//Update


}//Class
