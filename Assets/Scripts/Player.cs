﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

	[SyncVar]
	public Vector3 pos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (hasAuthority) {
			float horizontal = Input.GetAxis ("Horizontal");

			transform.Translate (horizontal * transform.right * Time.deltaTime, Space.Self);
			CmdUpdatePosition (transform.position);

		} else {
			transform.position = pos;
		}
	}

	[Command]
	public void CmdUpdatePosition(Vector3 p){
		if (!isServer) {
			return;
		}

		pos = p;
	}

	[Command]
	public void CmdGiveMeAuthority(GameObject obj, NetworkIdentity player){
		NetworkIdentity objId = obj.GetComponent<NetworkIdentity> ();
		objId.AssignClientAuthority (player.connectionToClient);

	}

	public override float GetNetworkSendInterval(){
		return .01f;
	}

}
